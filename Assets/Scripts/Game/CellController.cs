﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace TicTac.Game
{
    public class CellController : MonoBehaviour
    {
        public Type type;
        public GameObject signX;
        public GameObject signO;
        private Action<CellController> onClick;
        public int corX;
        public int corY;

        public void Init(Type type, int corX, int corY, Action<CellController> onClick)
        {
            this.type = type;
            this.corX = corX;
            this.corY = corY;
            this.onClick = onClick;
            RefreshCell();
        }

        public void RefreshCell()
        {
            if (type == Type.None)
            {
                signX.SetActive(false);
                signO.SetActive(false);
            }
            else if (type == Type.X)
            {
                signX.SetActive(true);
                signO.SetActive(false);
                GetComponent<Button>().interactable = false;
            }
            else if (type == Type.O)
            {
                signX.SetActive(false);
                signO.SetActive(true);
                GetComponent<Button>().interactable = false;
            }
        }

        public void ButtonClick()
        {
            ActionClick();
        }

        private void ActionClick()
        {
            if (onClick != null)
                onClick(this);
        }

        public void SetAction(Action<CellController> onClick)
        {
            this.onClick = onClick;
        }

        public void MakeTurn(Type type)
        {
            this.type = type;
            RefreshCell();
        }
    }
}
